
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', redirect: '/cards' },
      { path: '/cards', component: () => import('pages/Index.vue') },
      { path: '/list', component: () => import('pages/List.vue') },
      { path: '/list/:id', component: () => import('pages/List.vue'), props: route => ({ userId: parseInt(route.params.id) }) }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
