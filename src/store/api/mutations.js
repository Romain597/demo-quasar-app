import Vue from 'vue'

export function initializeUsers (state, users) {
  Vue.set(state, 'users', users)
}
