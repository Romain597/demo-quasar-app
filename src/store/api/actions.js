import { api } from 'src/boot/axios'
import { Notify } from 'quasar'

export function initializeUsers ({ state, dispatch }) {
  console.log('initializeUsers')
  if (state.users === null || state.users.length === 0) {
    dispatch('refreshUsers')
  }
}

export function refreshUsers ({ commit }) {
  console.log('refreshUsers')
  // call to an API
  return api.get('/users')
    .then((response) => {
      // console.log('Success')
      const users = []
      response.data.forEach((u) => {
        const user = {
          id: u.id,
          firstName: u.firstName,
          lastName: u.lastName,
          email: u.email
        }
        users.push(user)
      })
      commit('initializeUsers', users)
    })
    .catch((e) => {
      console.error('Loading failed :', e)
      Notify.create({
        type: 'negative',
        message: 'Erreur : ' + e.message
      })
    })
}

/* // Hardcoded data
  const users = [
    { id: 1, firstName: 'Thomas', lastName: 'Dacquin', email: 't.dascquin@gmail.com' },
    { id: 2, firstName: 'Yves', lastName: 'Dutronc', email: 'yves.dutronc@tutanota.com' },
    { id: 3, firstName: 'Mathieu', lastName: 'François', email: 'mathieu.francois@tutanota.com' },
    { id: 4, firstName: 'Bertrand', lastName: 'Mouget', email: 'bertrand.mouget@tutanota.com' },
    { id: 5, firstName: 'Françoise', lastName: 'Giroud', email: 'f.giroud@onlineformapro.com' }
  ];
  context.commit('initializeUsers', users); */
